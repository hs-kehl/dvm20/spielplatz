---
title: 'Installation eines Spielplatz-Sensors bei der Stadt Kehl'
author:
    - Selina Kohlhase
    - Cassandra Zimmermann
    - Christian Dinger
    - Chris Schwarzfischer
    - Fabian Verst
    
date: \today

link-citations: true
toc: true
toc-title: Inhaltsverzeichnis
toc-depth: 2
numbersections: true
lof: true
lot: true
listings: true
nocite: '@*'
titlepage: true
toc-own-page: true
titlepage-logo: "images/logo_HSKE.pdf"
logo-width: 130mm
footer-left: S. Kohlhase, C. Zimmermann, C. Dinger, C. Schwarzfischer, F. Verst 
csl: deutsche-gesellschaft-fur-psychologie.csl
tableTitle: Tabelle
figureTitle: Abbildung
header-includes:
    - \usepackage{float}
lofTitle: Abbildungsverzeichnis
lotTitle: Tabellenverzeichnis
---
\newpage

# Einleitung

LoRa-Was? Der Einsatz von Long-Range-Wide-Area-Network  (LoRaWan) wird immer beliebter in den Städten Deutschlands. Immer mehr Gemeinden entschließen sich dazu, eine sogenannte "Smart City" zu werden. Auch die Hochschule Kehl setzt hier einen Schwerpunkt in ihrem neuen, innovativen Studiengang "Digitales Verwaltungsmanagement" im Rahmen der Vorlesung "IT-Management".

In dem Projekt des Kurses IT-Management wird der Einsatz von LoRaWan-Sensoren in verschiedenen Anwendungsbereichen in der Stadt Kehl umgesetzt, um auch den Hochschulstandort ein wenig smarter zu machen. Die vorliegende Dokumentation beschreibt die Planung, Gestaltung und Installation eines LoRaWan-Sensors, welcher an einem Spielplatz platziert wird. Dabei sollen Echtzeit-Daten von Temperatur, Luftfeuchtigkeit, Personenanzahl, UV-Strahlung und Feinstaub gemessen werden. Dies soll der Bevölkerung einen Mehrwert in ihrer Freizeitgestaltung bieten. Familien können so beurteilen, ob sie zu den gegebenen Wetterverhältnissen den Spielplatz besuchen möchten oder lieber einen anderen Zeitpunkt wählen.

Die Projektumsetzung wird in OpenCode kollaborativ dokumentiert. Neben einer theoretischen Auseinandersetzung mit "Smart City" und dem "Internet der Dinge" beschäftigt sich dieser Projektbericht hauptsächlich mit der Zusammensetzung und Installation des Sensors (Hardware), sowie der Programmierung des Codes (Software) und Darstellung der Datenauswertung über ein Dashboard. Ebenso werden Absprachen mit der Stadt Kehl und innerhalb des Teams festgehalten.

\newpage

# Theoretische Grundlagen

## Smart City
Im Rahmen der Digitalisierung wird häufig nicht nur der digitale Veränderungsprozess innerhalb einer Behörde betrachtet, sondern auch der Blick nach außen, auf die Bevölkerung und das Zusammenleben, gerichtet. In diesem Zusammenhang fällt regelmäßig der Begriff „Smart City“. „Smart“ stammt aus dem Englischen und steht für schlau oder intelligent, „City“ – ebenfalls aus dem Englischen – für Stadt. Eine allgemeingültige Definition des Begriffs gibt es nicht. Die Autoren dieser Projektdokumentation verstehen hierunter jedoch eine intelligente Stadt, welche in der Regel mithilfe verschiedener Technologien effizienter, nachhaltiger und fortschrittlicher sein soll. Dies kann auf alle Bereiche einer Stadt ausgeweitet werden, welche sich in den folgenden sechs Anwendungsfeldern zusammenfassen lassen:[^SC-Anwendungsfelder]

\begin{figure}[H]
\centering
\includegraphics[width=3in]{./images/Smart_City_Anwendungsbereiche.png}
\caption{Anwendungsfelder von Smart City}
\end{figure} 

In diesen Anwendungsfeldern will die Smart City verschiedenen Herausforderungen begegnen und verfolgt dabei weitreichende Ziele, welche über die Digitalisierung hinausgehen. Zu nennen sind unter anderem ein geringerer Ressourcenverbrauch, eine verbesserte Lebensqualität aller sowie das Herstellen von Transparenz der Verwaltung über beispielsweise Open Data-Plattformen. Da dies insgesamt unter den Aspekt einer nachhaltigen Stadtentwicklung zu betrachten ist, ist es sinnvoll, die globalen Ziele für nachhaltige Entwicklung der Vereinten Nationen bei der Konzeption einer Smart City zu berücksichtigen.[^UNZiele]

Anwendungsfall einer Smart City kann beispielsweise die Überprüfung der Aufenthaltsqualität von Spielplätzen sein. Dies betrifft den Anwendungsbereich von Smart Environment, da es unter anderem zur Messung von Temperatur und Luftfeuchtigkeit oder der anwesenden Personen kommt. Die Erhebung solcher Daten führt zu Transparenz, erleichtert die Stadtplanung und Spielplatzentwicklung sowie eine nachhaltige Nutzung der verfügbaren Spielplätze.

## Internet of Things
Das „Internet of Things“ (IoT) ist eine Zusammenfassung von Technologien, welche die Kommunikation und Verbindung von Sensoren und weiteren technischen „Dingen“ über das Internet ermöglicht.[^SCBuch1] Mithilfe von Sensoren können innerhalb einer Stadt vorkommende Daten, zum Beispiel zu Klima (Temperatur, Luftfeuchtigkeit, etc.) oder Straßenverkehr (Auslastung von Straßen und Parkplätzen, Luftverschmutzung) einfach erfasst werden, da sie die entsprechenden Parameter messen können.
Damit eine Nutzung dieser erhobenen Daten möglich ist, werden sie beispielsweise in einer Anwendung dargestellt. Dabei ist die Entscheidung über die einzusetzende Technologie für die Kommunikation der Sensoren untereinander mit Bedacht zu treffen. Hierbei sind Kosten, Reichweite sowie weitere Voraussetzungen wie die Stromversorgung zu beachten. 
Werden die unterschiedlichen Technologien wie in Tabelle 1 verglichen, wird ersichtlich, dass die Technik des LPWANs (= „Low Power Wide Area Networks“) für den Anwendungsfall der Smart City am besten geeignet ist. Hierzu zählt auch LoRaWAN (= „Long Range Wide Area Network“).
Die LoRaWAN-Technologie wird von der LoRa Alliance® vorangetrieben. Sie stellt eine Non-Profit-Organisation dar, welche unter anderem Microsoft, Cisco oder Amazon zu ihren Mitgliedern zählt.[^LoRaMember] Ihr Ziel ist es, LoRaWAN als weltweite Technologie für den Einsatz von IoT zu etablieren und die hierfür notwendigen Standards weiterzuentwickeln. [^AboutLoRa]



\begin{table}[h]
\resizebox{\textwidth}{!}{%
\begin{tabular}{lllll}
\hline
 & \textbf{WAN} & \textbf{LAN} & \textbf{Cellular} & \textbf{LPWAN} \\ \hline
\textbf{Reichweite} & Langstrecke & Kurzstrecke & Langstrecke & Langstrecke \\ \hline
\textbf{Use Case} & Gerätevernetzung & Gerätevernetzung & \begin{tabular}[c]{@{}l@{}}Sprache, Machine-to-\\ Machine\end{tabular} & Internet der Dinge \\ \hline
\textbf{Vorteile} & \begin{tabular}[c]{@{}l@{}}hohe Bandbreite,\\ hohe Verfügbarkeit, Sicherheit\end{tabular} & mobil \& zu Hause einsetzbar & \begin{tabular}[c]{@{}l@{}}hohe Datenraten,\\ große Abdeckung\end{tabular} & \begin{tabular}[c]{@{}l@{}}niedriger Energieverbrauch via \\ Batterie, geringe Kosten, \\ Gebäudedurchdringung\end{tabular} \\ \hline
\textbf{Nachteile} & \begin{tabular}[c]{@{}l@{}}Kosten,\\ Flexibilität/Mobilität\end{tabular} & \begin{tabular}[c]{@{}l@{}}Batterielebensdauer,\\ keine Langstrecken\end{tabular} & \begin{tabular}[c]{@{}l@{}}Batterielebensdauer,\\ schlechte Gebäude-\\ durchdringung\end{tabular} & geringe Datenraten \\ \hline
\end{tabular}%
}
\caption{Vergleich der Technologien, angelehnt an (\protect\hyperlink{ref-SC-MadeinGermany}{Robert Koning, 2020}) S. 661} 
\end{table}

\newpage

Damit ein LoRaWAN-Netzwerk funktioniert, bedarf es neben den Sensoren, die die Daten erfassen, zusätzlich noch Gateways und ein LoRa-Netzwerk-Sever. Gateways können überall in der Stadt verteilt sein und sammeln die Werte mehrerer Sensoren. Sie sind vergleichbar mit einem Router im WLAN.[^SCBuch2] Von den Gateways aus werden die Messdaten im nächsten Schritt über Internetprotokolle wie TCP oder IP verschlüsselt an den Server übertragen. Dieser stellt die Daten unter anderem Anwendungen oder Dashboards zur Verfügung, welche von Bürgern und Betroffenen eingesehen werden können.[^SCBuch3]

  
Die vorgestellten Technologien können jedoch auch zu Angst und Ungewissheit führen. Wird beispielsweise die Anzahl an Personen mithilfe eines PAX-Sensors gemessen, stellt sich die Frage, ob hierbei auch ein personenbezogenes Datum erhoben wird, welches wiederum datenschutzrechtliche Probleme bedeuten würde. Da bei dieser Projektarbeit ebenfalls Personen auf diese Weise gezählt werden, soll auf diese Problematik näher eingegangen werden:
Um Personen beispielsweise auf einem öffentlichen Platz zählen zu können, werden die MAC-Adressen der vorhandenen Mobilgeräte erfasst und zeitweise gespeichert, sodass eine mehrfach Erfassung einer einzelnen Person vermieden wird. MAC-Adressen sind dabei physikalische Adressen, welche zu einer Netzwerkkarte zugeordnet werden können. Sie besteht aus 48 Bit und ist regelmäßig nicht einer einzelnen Person zuzuordnen, da hierfür weitere Informationen benötigt werden. Ob es im Einzelfall gegebenenfalls doch möglich ist, einen Rückschluss auf die einzelne Person zu ziehen, kann jedoch nicht abschließend beurteilt werden. Wird jedoch sichergestellt, dass die MAC-Adressen sofort anonymisiert und nachweislich nur für eine kurze Zeit gespeichert werden, ist davon auszugehen, dass es sich hierbei nicht um die Erhebung personenbezogener Daten handelt und somit zulässig ist.[^WD-BT] 	

\newpage

# Projektskizze
Nach diesen theoretischen Grundlagen, wird nun das Augenmerk auf die Vorbereitung des Projektes gelegt.

Mit besagten LoRaWan-Sensoren sollen die folgenden Daten erhoben werden: 

- Temperatur
- Luftfeuchtigkeit
- Personenzahl
- UV-Strahlung und
- Feinstaub

Diese Daten sollen gemessen werden, um Interessierten verlässliche Informationen zur aktuellen Situation rund um einen Spielplatz in der Stadt Kehl zu bieten. Die Autoren dieses Berichts würden sich für den Wasserspielplatz am Rhein entscheiden, da sich hier nach den Beobachtungen der Autoren vor allem im Sommer viele Eltern mit ihren Kindern aufhalten. Zudem besteht nur wenig Schutz vor Sonne, was vor allem für kleine Kinder gefährlich sein kann. Hierzu kann die Personenanzahl und die UV-Strahlung zusammen betrachten werden, um herauszufinden, ob aktuell viele Personen anwesend und damit die begehrten Schattenplätze rar sind. Die finale Abstimmung des Standortes muss jedoch noch mit der Stadt Kehl erfolgen.
Auf Grundlage der gesammelten Daten können die Personen in Zukunft entscheiden, ob der Spielplatz zum aktuellen Zeitpunkt aufgesucht werden sollte oder besser nicht. Durch die erhobenen Werte lässt sich die aktuelle Wetterlage am Rhein bestimmen und überblicken. Anhand der UV-Strahlung und der Feinstaubbelastung können mögliche Gesundheitsrisiken erkannt und vorgebeugt werden, zum Beispiel durch Auftragen von Sonnenschutz bei einem hohen UV-Wert. 

Nachdem die Sensoren durch städtische Mitarbeitende installiert wurden, können die ersten Testdaten unter Realbedingungen gesammelt werden. Diese Testdaten, wie beispielsweise die aktuelle Personenanzahl, die mithilfe eines PAX-Counters ermittelt wird, müssen verifiziert werden. Da Eltern mit ihren Kindern auf dem Spielplatz sein werden, muss die gemessene Zahl der erkannten Wifi-Geräte um eine Zahl X angepasst werden. Um diesen unbekannten Wert zu ermitteln, müssen IST-Werte im Realbetrieb händisch erfasst werden. 

\newpage

# Vorbereitung

Um ein Projekt erfolgreich umzusetzen, muss Verschiedenes vorbereitet werden. Im folgenden Kapitel werden die notwendigen Vorbereitungen für dieses Projekt beschrieben.

## Löt-Workshop

Der erste praktische Schritt des Projektes ist das Löten der Sensoren, damit diese funktionsfähig werden und geeignete Daten senden können. Um die Fähigkeit des Lötens zu erlernen, wurde ein Löt-Workshop veranstaltet.

Im ersten Teil dieses Workshops werden die Grundlagen des Lötens vermittelt. Dazu zählt unter anderem die richtige Handhabung des Lötkolbens oder die korrekte Anwendung des Lötzinns sowie notwendige Schutzvorkehrungen wie die Einhaltung des Brandschutzes. Damit sich alle Gruppenmitglieder diese Grundlagen aneignen können, wird im ersten Teil von jedem ein kleiner Anhänger mit drei Leuchtdioden hergestellt, welcher am Ende in unterschiedlichen Farben leuchten soll.

\begin{figure}[H]
\centering
\includegraphics[width=14cm]{./images/StarterKit_collage.jpg}
\caption{Starterkit}
\end{figure} 

Nachdem diese Grundlagen erworben wurden, werden die tatsächlichen Sensoren gelötet. Dabei muss eine Platine auf den zur Verfügung gestellten (Octopus-)Sensor gelötet werden, sodass Temperatur, Luftfeuchtigkeit und Personenanzahl korrekt gemessen werden können. Dieser wird mit zwei weiteren Sensoren, welche für die Messung des Feinstaubs und der UV-Strahlung zuständig sind, auf der Hauptplatine angebracht. Damit ist der Mikrocontroller einsatzbereit.

\begin{figure}[H]
\centering
\includegraphics[width=14cm]{./images/AlleSensoren_Collage.jpg}
\caption{Sensoren}
\end{figure} 

Im letzten Teil des Workshops wird ein Programm für den Mikrocontoller geschrieben. Dazu wird die Entwicklungsumgebung "Arduino IDE" verwendet. Diese Entwicklungsumgebung wird auf dem Computer ausgeführt, das Programm geschrieben und anschließend auf den Mikrocontroller aufgespielt. Damit keine weitreichenden Programmierkenntnisse Voraussetzung sind, stellt die "Arduino IDE" ein Werkzeug namens "ArduBlock" zur Verfügung. Durch eine grafische Oberfläche kann Code durch "Zusammenstecken" von Codeblöcken erzeugt werden. Der fertige Code wird durch eine USB-Verbindung zum Mikrocontroller übertragen.

Nachdem der Code auf den Mikrocontroller übertragen ist, können die unterschiedlichen Sensoren getestet werden. Dieser Test konnte in soweit erfolgreich gestaltet werden, dass alle Sensoren Daten lieferten. Im nächsten Schritt müssen die gelieferten Daten entsprechend interpretiert werden. Zum Beispiel liefert der UV-Sensor ein elektrisches Signal, welches sich verändert, je nachdem welche Intensität an UV-Strahlung besteht. Dieses Signal muss in einen UV-Index bzw. Verhaltenshinweis umgewandelt werden, sodass dieses Datum einen nachvollziehbaren Mehrwert für Bürgerinnen und Bürger bietet.

\newpage

## Installation und Kommunikation

Die fertiggestellte Sensorik soll in einem nächsten Schritt an ihrem Einsatzort installiert werden. Der Sensor soll Daten von einem Spielplatz senden und somit interessierten Bürgerinnen und Bürgern Informationen über die Wetterlage, Spielplatzbelegung und Feinstaubbelastung liefern. Hierzu wird ein Spielplatz im Stadtgebiet Kehl ausgewählt. Kriterien für die Auswahl sind die Entfernung zur Hochschule Kehl, weil die Daten nur bis zu einer bestimmten Strecke gesendet werden können, und die Beliebtheit des Spielplatzes, sodass möglichst viele Menschen von den gesammelten und aufbereiteten Daten profitieren können.

Hierzu verständigt sich die Gruppe mit der Stadt Kehl, um eine gemeinsame Lösung zu finden. Schließlich ist die Wahl nach kurzen Beratungen auf den Wasserspieplatz am Rhein in der Gustav-Weis-Straße 19 gefallen. Die Entscheidung fiel auf diesen Spielplatz, da dieser im Sommer sehr gut frequentiert ist und die Daten zumindestens im Sommer für viele Menschen hilfreich sein werden. Die Begründung für die gewählten Daten sind im Kapitel [Projektskizze](#projektskizze) aufgeführt.

Nachdem die Sensorik in den Räumlichkeiten und auf dem Gelände der Hochschule für öffentliche Verwaltung Kehl getestet wird, kann sie am tatsächlichen Einsatzort installiert werden. Bei den Tests gab es einige Probleme, welche durch Ausprobieren verschiedener Lösungsansätze und mit Hilfe des Dozenten Herr Itrich größtenteils gelöst werden konnten. Damit der Sensor Daten sammeln und vom Spielplatz zum Gateway an der Hochschule übertragen kann, benötigt der Sensor Strom. Der Stromanschluss soll dabei durch eine Straßenlaterne realisiert werden. Falls der Stromanschluss im Zeitraum dieses Semesters nicht installiert werden kann, wird vorläufig ein Akku für die benötigte Stromversorgung sorgen.

\newpage



# Projektumsetzung und Auswertung

In diesem Kapitel wird sich mit dem Aufbau des Dashboards, Vorgang zum Testen der Sensoren und den ersten Auswertungen, die an den Testmessdaten vorgenommen werden konnten, befasst.

## Datenauswertung

Im Zuge der Entwicklung des Programmcodes mit Ardublock wird regelmäßig getestet, ob die Daten, die angezeigt werden, realistisch erscheinen. Dazu werden externe Messgeräte, wie ein Thermometer für Temperatur und Luftfeuchtigkeit, sowie lokale Wetterdaten für die UV-Werte herangezogen. Auch werden Tests für den Feinstaub- und PAX-Sensor durchgeführt. Der Feinstaubsensor wird an verschiedenen Orten innerhalb und außerhalb des Hochschulgebäudes und an der Auspuffanlage eines laufenden PKWs getestet. Für den PAX-Counter werden verschiedene Reichweiten und Einstellungen ausgetestet. Um Reichweiten zu simulieren, die ähnlich groß wie der Spielplatz sind, auf dem der Personenzahlsensor angebracht werden soll, eignet sich die große Wiese zwischen der Hochschule und dem Wohnheim. Dort ist ein großer Freiraum ohne Fremdsignale oder Hindernisse, die den Test stören könnten. Es wurden methodisch mit fünf W-Lan-Signalen verschiedene Entfernungen von dem Sensor getestet. Dabei gab es Schwierigkeiten, die genaue Anzahl von W-Lan-Geräten in der Nähe des Sensors festzustellen. Der PAX-Counter gab nur einen ungefähren Messwert zurück. Für die Nutzenden ist es also kein Wert, der die genaue Personenzahl angezeigt, sondern nur einen grober Richtwert über die Fülle des Spielplatzes darstellt.

\begin{figure}[H]
\centering
\includegraphics[width=15cm]{./images/sensordaten_roh.png}
\caption{Sensorwertausgabe in Programmierumgebung}
\end{figure} 


Durch die Tests an dem Sensor fiel auf, dass die Ausgaben des UV-Wert-Sensors nicht mit den zu erwarteden UV-Werten übereinstimmten. Dazu wurden Nachforschungen in der Dokumentation des Herstellers des eingebauten UV-Sensors durchgeführt. Diese ergaben, dass die Ausgabe des Sensors ein Spannungswert ist, welcher erst umgerechnet werden muss. 
Im Internet gefundene und eigenständig errechnete Formeln zur Umrechnung der Werte führten nicht zu realistischen UV-Werten. 

\newpage

Ein weiterer Lösungsansatz ist es, verschiedenen Spannungsbereichen feste UV-Werte zuzuordnen.
Die Tabelle aus dem Code von Github-User "ThomasTransboundaryYan"[^UVWerteUmrechnung], welcher mit dem selben Typ von UV-Sensor arbeitet, ergibt:  


1. Ein Messwert von 6/7 im Raum spricht für einen UV-Wert von 0  
2. Ein Messwert von 60/61 in der Sonne spricht für einen UV-Wert von 1  

Diese UV-Werte korrelieren mit den Wetter-Daten zur Zeit der Datenerhebung. Diese Werteumrechnung wird in das Dashboard übernommen, damit die UV-Wertumrechnung angepasst werden kann, falls diese nicht mehr stimmt, sobald der Sensor angebracht ist:

```javascript
if (value < 50)
    return 0;
else if (value < 227)
    return 1;
else if (value < 318)
    return 2;
else if (value < 408)
    return 3;
else if (value < 503)
    return 4;
else if (value < 606)
    return 5;
else if (value < 696)
    return 6;
else if (value < 795)
    return 7;
else if (value < 881)
    return 8;
else if (value < 976)
    return 9;
else if (value < 1079)
    return 10;
else
    return 11;
```

Besonders hohe Sensorwerte können derzeit aufgrund der Jahreszeit nicht getestet werden, dies muss über einen längeren Zeitraum im Sommer durchgeführt werden.

Da die anderen Sensoren in verschiedenen Umgebungen und Umständen getestet wurden, können wir davon ausgehen, dass diese auch funktionieren und korrekte Daten ausgeben, sobald der Sensor an der Laterne auf dem Spielplatz angebracht ist. Wie eine langfristige Aussetzung gegenüber den Witterungsverhältnissen die Sensormesswerte beeinflusst, konnte vorher nicht getestet werden. 

\newpage

Fragen, welche die Autoren sich stellen, sind:

- Verfälscht Kondenswasser an der Plexiglasscheibe die Luftfeuchtigkeitsmesswerte?
- Verfälscht Schnee auf dem Sensor die UV-Messwerte?
- Sorgt die Plexiglasscheibe für einen Treibhauseffekt, welcher Temperaturmesswerte verfälscht?
- Wie verhält sich die Temperatur in der Sensorbox zum Klima außerhalb? 

Antworten auf diese Fragen können erst im  Realbetrieb erhalten werden.

## Dashboard

\begin{figure}[H]
\centering
\includegraphics[width=15cm]{./images/dashboard_bsp.png}
\caption{Dashboard}
\end{figure}

Das Dashboard, welches im mhascaro Portal erstellt wird, besteht aus 5 Graphen für die Messwerte, welche wir standardmäßig über einen Zeitraum von den vergangenen 5 Stunden anzeigen lassen. Die Nutzenden haben im späteren Verlauf die Möglichkeit, diesen Zeitraum flexibel anzupassen. Zudem sind aktuelle Werte für UV-Strahlung und Temperatur als oberste Blöcke auf dem Dashboard angezeigt. 

Auch beinhaltet das Dashboard eine "Speedskala", welche umfunktioniert wird, damit diese die Feinstaubkonzentration darstellt. Die Grenzwerte werden für die Nutzenden anhand einer Farbskala, welche sich von weiß, zu gelb und auf höchster Stufe zu rot verändert, einfach und übersichtlich dargestellt. Die Autoren haben Informationen über die Feinstaubgrenzwerte bei einer Bürgerreferentin bei dem Ministerium für Verkehr und einem Forscher namens Benjamin Aretz, welcher sich mit Feinstaubbelastung auseinandersetzt, angefragt. 

\newpage

Herr Aretz empfiehlt, sich an die strengeren Grenzwerte der WHO zu halten, deshalb wurde sich für eine Einteilung in die Warnbereiche von 5 µg/m³ (WHO 2021), 10 µg/m³ (WHO 2005) und 25 µg/m³(EU-Grenzwert) enschieden.

\begin{figure}[H]
\centering
\includegraphics[width=3in]{./images/Speedgauge_feinstaub.png}
\caption{Feinstaub Speedskala}
\end{figure}

Für die Verbildlichung der Messwerte im Dashboard mussten Achsenwerte, Maximal- und Minimalgrößen und der Anzeigezeitraum für die Graphen gewählt werden. 
In der Gruppe wurde sich dazu über sinnvolle Werte für die Anzeige abgestimmt und dazu eine passende Skalierung gewählt, damit die Graphen gut lesbar sind. Diese sind:

- Personenauslastung: Wert von 0 - 100. Dargestellt in 10er Schritten.
- Temperatur: Reichweite von -10°C bis 40°C. Dargestellt in 5er Schritten.
- Luftfeuchtigkeit: Wert von 0% bis 100%. Dargestellt in 10er Schritten.
- Feinstaub: Werte von 0 - 50 µg/m³. Dargestellt in 5er Schritten.
- UV-Wert: Werte von 0 - 11. Dargestellt in 1er Schritten.

Eine weitere Funktion, die das mhascaro Portal bietet, ist die direkte Nachbearbeitung der Sensormessdaten im Dashboard, sodass ein Aktualisieren des Sensorcodes nicht zwingend notwendig ist. So können auch an dem angebrachten Sensor noch Änderungen vorgenommen werden, ohne vor Ort neuen Code auf die Hardware aufspielen zu müssen. 

Auch wird eine Funktion in den Code auf der Hardware eingebaut, mit welcher der Sensor ferngesteuert neugestartet werden kann. Dazu wurde sich entschieden, da bei einem der Tests des Sensor ein Fehler auftrat, bei welchem falsche Daten übertragen wurden. Dieser konnte nur durch Ausschalten des Sensors behoben werden. Um dies zu tun muss von einem "The Things Stack"-Account ein "01" Befehl an den Sensor gesendet werden.

\begin{figure}[H]
\centering
\includegraphics[width=11cm]{./images/ardublock_klein.png}
\caption{Code in Ardublock}
\end{figure}


Der gesamte Code kann dem Anhang entnommen werden.

Der Sensor wurde mit einer großen Menge an Funktionalitäten ausgestattet. Sobald dieser Messwerte über einen längeren Zeitraum sammelt, können - falls nötig - verschiedene Einstellungen am Dashboard nachjustiert werden. Zum jetzigen Stand hat der Sensor jedoch alle Funktionen und Fähigkeiten auf dem Niveau erreicht, welche sich die Autoren zu Beginn des Projektes vorgestellt haben.


Zu finden ist das Dashboard unter: [mhascaro-Portal](https://portal.mhascaro.com/dashboard/ef1ea5c0-748c-11ed-8ae8-0d887c12d9af?publicId=88624cd0-9596-11ed-8ae8-0d887c12d9af)

\newpage

# Projektarbeit
Auch wenn das Projekt hauptsächlich aus der Installation des Sensors besteht, gibt es andere Aspekte, die maßgeblich über Erfolg und Misserfolg entscheiden können. In diesem Kapitel sollen wesentliche Punkte zur Arbeit in OpenCode und zur Teamarbeit allgemein festgehalten werden. Die Arbeit mit OpenCode ist für alle Teammitglieder neu und musste im Projektverlauf erlernt werden. Wichtige Erkenntnisse und Fragen, die über das Semester hinweg aufkamen, werden im ersten Teil der Projektarbeit reflektiert. Absprachen zur Zusammenarbeit im Team und worauf aus Sicht der Autoren besonders Wert gelegt werden muss, werden im zweiten Teil genauer betrachtet.

## Arbeiten mit OpenCode

### Wie wird ein Merge Request erstellt?
1. Die Änderung wird vorgenommen
2. Klick auf "Create commit..."
3. Bei "Commit Message" kurz beschreiben, was gemacht wurde
4. [x] create a new Branch
5. Branch sinnvoll benennen
6. [x] Start a new Merge Request (Häkchen rein)
7. Klick auf "Commit"

Nun erscheint ein Vorschlag zu einem neuen Merge Request.
Hier sollen Titel und Beschreibung ergänzt werden.
Alles weitere kann bei den Standard-Einstellungen belassen werden.

1. [x] Delete source branch when merge request is accepted
2. [ ] squash commits when merge request is accepted

Anschließend wird der Merge Request erstellt.

### Wie kann eine PDF der Dokumentation erzeugt werden?
1. Klick auf "CD/CI" im linken Reiter
2. Klick auf "Pipelines"
3. Klick auf "Download"
4. Datei im Zielpfad entpacken
5. Der entpackte Ordner enthält die PDF

### Wie wird ein Bild hochgeladen, das in der Dokumentation verwendet wird?
Hier gibt es zwei Möglichkeiten.

Entweder:

1. Das Bild wird im Main-Branch hochgeladen

2. Ein Merge Request wird geöffnet, in welchem dann auf das Bild zugegriffen wird

Oder:

1. Ein Merge Request wird geöffnet

2. Innerhalb des Merge Requests wird das Bild hochgeladen

### Markdown-Feinheiten

*Fußnoten*: Fußnoten müssen im Fließtext und im Literaturverzeichnis identisch sein. Hierbei ist darauf zu achten, sich nicht zu vertippen, die Syntax zu berücksichtigen, den gleichen Namen in der Referenz zu verwenden und die Groß- und Kleinschreibung zu beachten.

```
[^FussnoteBeispiel] <- im Fliesstext

[^FussnoteBeispiel]: Quellenangabe <- im Literaturverzeichnis
```

*Quellen*: Mit dem Standard-Quellenverzeichnis, das in OpenCode hinterlegt ist, hat die Zitierweise nicht funktioniert. Erst durch das Einbinden der "Zitierweise der deutschen Gesellschaft für Psychologie" konnten Quellen richtig angezeigt werden.

### Kleine Fragen zur Arbeit mit OpenCode
Frage 1: Ist es bei einem Merge Request möglich, mehrere Teammitglieder als Reviewer einzustellen?

Nein.

Frage 2: Bei der Erstellung eines Merge Requests gibt es die Möglichkeit "Approval is optional". Kann Approval auch nicht-optional gemacht werden?

In der kostenlosen Version von OpenCode ist dies nicht möglich.

\newpage

### Anmerkungen zur Arbeit mit OpenCode
- Die Prüfungsleistung sieht die Abgabe dieser Projektdokumentation vor. Dabei ist der Umfang der Abgabe an eine bestimmte Seitenzahl gekoppelt. Während der Erstellung der Readme.md in Markdown sind Seitenzahlen und Absätze nicht erkennbar. Die tatsächliche Länge der Arbeit kann so nur schwer abgeschätzt werden und muss umständlich über die regelmäßige Ausgabe als PDF überprüft werden.
- Das Einfügen von Bildern in den Fließtext gestaltet sich als umständlich. Wie groß die Bilder in der PDF angezeigt werden und wo genau sie platziert werden, lässt sich mit den Standardeinstellungen nur schwer steuern. Auch durch eine spätere Lösung, mit welcher die Bilder an einer gewissen Stelle im Fließtext fixiert werden können, bleibt das Problem der Bildergröße bestehen. Zwar kann diese individuell angepasst werden, doch muss jedes Mal die Pipeline abgewartet werden und das Artefakt gedownloadet werden, bevor in der PDF überprüft werden kann, ob die Darstellung wie gewünscht ist.
- Die Erstellung einer Tabelle über Markdown/Opencode funktioniert zunächst einfach und intuitiv. Allerdings war die automatische Festlegung der Spaltengröße ungeschickt, da sich diese nach der Spaltenüberschrift und nicht nach dem Spalteninhalt richtete. Über einen Umweg mithilfe des LaTeX Generator konnte dieses Problem behoben werden. So konnte die Tabelle nach den eigenen Vorstellungen dargestellt und letztendlich in die Projektdokumentation übernommen werden.


## Arbeiten im Team

### Projektmanagement

Es finden regelmäßige Teambesprechungen statt, in welchen über das Projekt gesprochen wird. Im ersten Meeting wurden die Aufgaben verteilt. Die einzelnen Abschnitte dieses Projektberichts werden federführend von den einzelnen Teammitgliedern dokumentiert. An den Projektschritten selbst wird gemeinsam gearbeitet. Die dokumentierten Inhalte werden von allen Teammitgliedern korrekturgelesen und inhaltlich ggf. ergänzt. Größere Änderungen/Anmerkungen, Entscheidungen oder Problemstellungen im Projektverlauf werden in den Teamsitzungen besprochen. Der Termin für die nächste Sitzung wird immer am Ende der Sitzung gemeinsam festgelegt.

Den Großteil der Absprachen kann während der Vorlesungszeit getroffen werden.
Kleinere Absprachen werden auch über die WhatsApp-Gruppe getroffen.

\newpage

### Absprache zu Merge Requests

1. Ein Teammitglied sichert sich in Absprache mit dem Team den nächsten freien "Merge Request-Slot". So kann vermieden werden, dass mehrere neue Merge Requests gleichzeitig gestartet werden.
2. Das Teammitglied schreibt und veröffentlicht den Merge Request
3. Da OpenCode keine automatischen Nachrichten bei der Veröffentlichung von Merge Requests verschickt, informiert das Teammitglied das Team per WhatsApp über die Veröffentlichung des neuen Merge Requests.
4. Jedes Teammitglied kontrolliert den Merge Request und hinterlässt ggf. Kommentare. Hierbei ist darauf zu achten, "Start a Review" statt "Add comment now" auszuwählen, um alle Kommentare gebündelt an den Ersteller des Merge Requests zu senden.
5. Jedes Teammitglied hat sein Approval zu geben. So hat der Ersteller des Merge Requests einen Überblick, wann alle Teammitglieder den Merge Requests kontrolliert haben.
6. Wenn Änderungen vorgeschlagen wurden, pflegt diese der Ersteller des Merge Requests ein. Hierbei werden diese auf den gleichen Merge Request commitet. (Commit to ... branch; Haken raus bei "start a new Merge Request)
7. Wenn alle Änderungen eingepflegt wurden, kann gegebenenfalls eine zweite Rückmeldeschleife sinnvoll sein. (Springe zu Punkt 3)
8. Sobald alle Teammitglieder approved haben und alle Kommentare gelöst sind, merged der Ersteller des Merge Requests diesen.

Das Öffnen mehrerer Merge Requests/Branches gleichzeitig kann zu Fehlern führen. Um die Fehlerquelle an dieser Stelle möglichst gering zu halten, wurde sich auf dieses Vorgehen geeinigt.


\newpage

# Fazit

LoRaWan! Eine moderne Technik, die zunehmend an Bedeutung in unserer Gesellschaft gewinnt. Dieses Projekt konnte einen weiteren Beispielsfall aufzeigen, wie diese Technik sinnvoll eingesetzt werden kann.

Nachdem ein funktionstüchtiger Sensor hergestellt und programmiert wurde, kam es bei der Auswertung der einzelnen Daten zu Herausforderungen. Einige Daten-Ausgaben konnten auf den ersten Blick nicht richtig interpretiert werden. Hierfür mussten individuelle Lösungen gefunden werden. Vor allem der PAX-Sensor verhielt sich für die Forschungsgruppe schwer nachvollziehbar, wodurch dieser erst im Echtbetrieb feinjustiert werden kann.

Insgesamt liefert das Dashboard zuverlässige Daten für: Temperatur, UV-Strahlung, Feinstaub und Luftfeuchtigkeit. Der Sensor konnte noch nicht am Spielplatz installiert werden. Die Daten werden aktuell alle 30.000 Milisekunden = 0,5 Minuten aktualisiert. Dies ist noch ein Test-Intervall. 

Rückblickend betrachtet war die Kommunikation mit der Stadt Kehl nicht optimal. Unterschiedliche Erwartungen und Absprachen an denen nicht alle Mitwirkende beteiligt waren, führten dazu, dass Missverständnisse entstanden sind. 
Um dies zu vermeiden, wäre ein Kick-Off-Meeting zu Beginn des Projekts von Vorteil gewesen. Hier hätten die einzelnen Vorstellungen und Zielvorstellungen von Anfang an klar definiert und festgehalten werden können, um Missverständnissen vorzubeugen. Zusätzlich hätte dies eine engere Zusammenarbeit während des Projektverlaufs ermöglicht.

An den aktuellen Stand des Projekts anknüpfend, müssen noch einige Schritte bearbeitet werden, bevor der Sensor einen Nutzen für die Allgemeinheit hat:

Für die Installation wurde bereits eine Laterne ausgewählt, an welcher der Sensor außerhalb der Reichweite der allgemeinen Bevölkerung angebracht werden kann. Diese könnte dem Sensor auch als Stromquelle dienen. Solange dies nicht umgesetzt werden kann, bekommt der Sensor eine unabhängige Stromversorgung über einen Solar-Akku.

Sobald der Sensor am Spielplatz platziert wird, muss eine Überprüfung der Daten erfolgen. Die Interpretation des PAX-Sensors muss vor Ort abgeschlossen werden. Ebenso muss dann das Intervall zum Senden der Daten auf 30 Minuten verlängert werden.

Über die Konkretisierung der Daten hinaus, sollte sich darüber Gedanken gemacht werden, wie die Kommunikation mit den Bürgerinnen und Bürgern gestaltet werden kann. Folgende Fragen können hierbei berücksichtigt werden: Wie erfahren diese von dem Sensor/Dashboard? Reicht ein Info-Text auf der Homepage der Hochschule? Welche Kanäle hat die Stadt Kehl selbst, um die Information zu verbreiten (Amtsblatt, Website, Social Media, etc.)?

\newpage

# Anhang
```c
/* Disclaimer IoT-Werkstatt CC 4.0 BY NC SA 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. For Ardublock see the
 GNU General Public License for more details. */
#include <Seeed_HM330X.h>
#include <ESP8266WiFi.h>
#include <WiFi_Sniffer.h>
#include <bsec.h>
#include <Wire.h>
#include <Ticker.h>
#include <lmic.h>
#include <hal/hal.h>
#define LORA_TX_INTERVAL 10
#define LORA_DOWNLINK_ENABLE
#define LORA_DEEPSLEEP

int feinstaub = 0 ;
HM330X HM330sensor; // www.seeed.cc, Author: downey

int HM330sensor_ready=0; // for init after deep-sleep

// Feinstaubsensor HM330 Copyright (c) 2018 Seeed Technology Co., Ltd. 
int readFeinstaubHM330(int chan) {
  if(!HM330sensor_ready) {
    HM330sensor_ready = !HM330sensor.init(); 
    delay(1000); 
  }
  u8 data[30]; // Puffer für Antwort
  int i,val;
  u8 sum=0;
  HM330sensor.read_sensor_value(data,29);
  if (chan == 2) i=6; 
  else i=7;
  val = (u16)data[i*2]<<8|data[i*2+1];
  for(int i=0;i<28;i++) { // checksum
    sum+=data[i];
  }
  if(sum!=data[28]) val=-1; 
  return val;
}

int uv_strahlung = 0 ;
int pax = 0 ;
unsigned int Sniff_channel = 1;
//------------ WiFi-Sniffer,  This software is based on the work of Andreas Spiess, https://github.com/SensorsIot/Wi-Fi-Sniffer-as-a-Human-detector//                            and Ray Burnette: https://www.hackster.io/rayburne/esp8266-mini-sniff-f6b93a 
int WiFiPaxCounter(int MinRSSI,  int timeout, int8 mychannel,String myMAC,int mydisplay) {
  int mycount=0;
  int randMAC=0;
  int ChanMin = 1, ChanMax =13; // europe channel 1-13, Japan 1-14
  if (mychannel > 0) {
    ChanMax = mychannel; 
    ChanMin = mychannel;
  };
  if (mychannel < 0) {
    randMAC = mychannel;
  };
  wifi_set_promiscuous_rx_cb(promisc_cb);   // Set up promiscuous callback
  Sniff_channel = ChanMin;
  wifi_set_channel(Sniff_channel);
  wifi_promiscuous_enable(true);
  for (Sniff_channel = ChanMin; Sniff_channel <= ChanMax; Sniff_channel++) {
    wifi_set_channel(Sniff_channel);
    delay(300);            // 300 ms per channel
  }
  wifi_promiscuous_enable(false);
  mycount = SnifferCountDevices(MinRSSI,timeout,myMAC,randMAC,mydisplay); // Anzeige/zaehlen der Clients 
  return mycount;
}

int temperatur = 0 ;
/* 
 Bosch BSEC Lib, https://github.com/BoschSensortec/BSEC-Arduino-library
 The BSEC software is only available for download or use after accepting the software license agreement.
 By using this library, you have agreed to the terms of the license agreement: 
 https://ae-bst.resource.bosch.com/media/_tech/media/bsec/2017-07-17_ClickThrough_License_Terms_Environmentalib_SW_CLEAN.pdf */
Bsec iaqSensor;     // Create an object of the class Bsec 
Ticker Bsec_Ticker; // schedule cyclic update via Ticker 
const uint8_t bsec_config_iaq[] = {
#include "config/generic_33v_3s_28d_2d_iaq_50_200/bsec_iaq.txt"
};

// ------------------------   Helper functions Bosch Bsec - Lib 
void checkIaqSensorStatus(void)
{ 
  String output; 
  if (iaqSensor.status != BSEC_OK) {
    if (iaqSensor.status < BSEC_OK) {
      output = "BSEC error code : " + String(iaqSensor.status);
      for (;;) {
        Serial.println(output);
        delay(500);
      } // Halt in case of failure 
    } 
    else {
      output = "BSEC warning code : " + String(iaqSensor.status);
      Serial.println(output);
    }
  }

  if (iaqSensor.bme680Status != BME680_OK) {
    if (iaqSensor.bme680Status < BME680_OK) {
      output = "BME680 error code : " + String(iaqSensor.bme680Status);
      for (;;){
        Serial.println(output);
        delay(500);
      }  // Halt in case of failure 
    } 
    else {
      output = "BME680 warning code : " + String(iaqSensor.bme680Status);
      Serial.println(output);
    }
  }
}

// Housekeeping: scheduled update using ticker-lib
void iaqSensor_Housekeeping(){  // get new data 
  iaqSensor.run();
}

int luftfeuchtigkeit = 0 ;
// LoraWAN Copyright (c) 2015 Thomas Telkamp and Matthijs Kooijman
// (c) 2018 Terry Moore, MCCI
// https://github.com/mcci-catena/arduino-lmic
// -------- LoRa PinMapping FeatherWing Octopus
const lmic_pinmap lmic_pins = {  
  .nss = 2,                            // Connected to pin D
  .rxtx = LMIC_UNUSED_PIN,             // For placeholder only, Do not connected on RFM92/RFM95
  .rst = LMIC_UNUSED_PIN,              // Needed on RFM92/RFM95? (probably not) D0/GPIO16 
  .dio = {
    15, 15, LMIC_UNUSED_PIN         }
};

static const u1_t PROGMEM DEVEUI[8]={
  0x96,0x7C,0x05,0xD0,0x7E,0xD5,0xB3,0x70};
void os_getDevEui (u1_t* buf) { 
  memcpy_P(buf, DEVEUI, 8);
}

static const u1_t PROGMEM APPEUI[8]={
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};
void os_getArtEui (u1_t* buf) { 
  memcpy_P(buf, APPEUI, 8);
}

static const u1_t PROGMEM APPKEY[16]={
  0x45,0xDB,0x0B,0xF8,0x64,0x7D,0xBA,0xB9,0xCF,0x74,0x51,0xAD,0xDC,0xD7,0x8E,0x2A};
void os_getDevKey (u1_t* buf) {  
  memcpy_P(buf, APPKEY, 16);
};

volatile int LoRaWAN_Tx_Ready   = 0; // Flag for Tx Send 
long         LoRaWAN_ms_Wakeup  = 0; // ms at start message
long         LoRaWAN_ms_EmExit  = 0; // max. ms  

int LoRaWAN_Rx_Payload = 0 ;
int LoRaWAN_Rx_Port = 0 ;
// Berechne CRC-Prüfsumme für RTC-RAM 
uint32_t RTCcalculateCRC32(const uint8_t *data, size_t length) {
  uint32_t crc = 0xffffffff;
  while (length--) {
    uint8_t c = *data++;
    for (uint32_t i = 0x80; i > 0; i >>= 1) {
      bool bit = crc & 0x80000000;
      if (c & i) {
        bit = !bit;
      }
      crc <<= 1;
      if (bit) {
        crc ^= 0x04c11db7;
      }
    }
  }
  return crc;
}
//--------------------------  Load/Store LoRa LMIC to RTC-Mem 
void LoadLMICFromRTC() {
  lmic_t RTC_LMIC;
  uint32_t crcOfData;
  if (sizeof(lmic_t) <= 512) {
    ESP.rtcUserMemoryRead(1, (uint32_t*) &RTC_LMIC, sizeof(RTC_LMIC));
    ESP.rtcUserMemoryRead(0, (uint32_t*) &crcOfData, sizeof(crcOfData));
    uint32_t crcOfData_RTC = RTCcalculateCRC32((uint8_t*) &RTC_LMIC, sizeof(RTC_LMIC));
    if (crcOfData != crcOfData_RTC) {
      Serial.println("CRC32 in RTC memory doesn't match CRC32 of data. Data is probably invalid!");
    } 
    else {
      Serial.print(F("load LMIC from RTC, FrameCounter =  "));
      LMIC = RTC_LMIC;
      Serial.println(LMIC.seqnoUp);     
    }  
  } 
  else {
    Serial.println(F("sizelimit RTC-Mem, #define LMIC_ENABLE_long_messages in config.h"));
  }
} 

void SaveLMICToRTC(int deepsleep_sec) {
  if (sizeof(lmic_t) <= 512) {
    Serial.println(F("Save LMIC to RTC and deepsleep"));
    unsigned long now = millis();
    // EU Like Bands
#if defined(CFG_LMIC_EU_like)
    // Serial.println(F("Reset CFG_LMIC_EU_like band avail"));
    for (int i = 0; i < MAX_BANDS; i++)
    {
      ostime_t correctedAvail = LMIC.bands[i].avail - ((now / 1000.0 + deepsleep_sec) * OSTICKS_PER_SEC);
      if (correctedAvail < 0)
      {
        correctedAvail = 0;
      }
      LMIC.bands[i].avail = correctedAvail;
    }

    LMIC.globalDutyAvail = LMIC.globalDutyAvail - ((now / 1000.0 + deepsleep_sec) * OSTICKS_PER_SEC);
    if (LMIC.globalDutyAvail < 0)
    {
      LMIC.globalDutyAvail = 0;
    }
#else
    //Serial.println(F("No DutyCycle recalculation function!"));
#endif
    // Write to RTC
    uint32_t crcOfData = RTCcalculateCRC32((uint8_t*) &LMIC, sizeof(LMIC));
    ESP.rtcUserMemoryWrite(1, (uint32_t*) &LMIC, sizeof(LMIC));
    ESP.rtcUserMemoryWrite(0, (uint32_t*) &crcOfData, sizeof(crcOfData));
  } 
  else {
    Serial.println(F("sizelimit RTC-Mem, #define LMIC_ENABLE_long_messages in config.h"));
  }
} 
void onEvent (ev_t ev) {
  Serial.print(os_getTime());
  Serial.print(": ");
  switch(ev) {
  case EV_SCAN_TIMEOUT:
    Serial.println(F("EV_SCAN_TIMEOUT"));
    break;
  case EV_BEACON_FOUND:
    Serial.println(F("EV_BEACON_FOUND"));
    break;
  case EV_BEACON_MISSED:
    Serial.println(F("EV_BEACON_MISSED"));
    break;
  case EV_BEACON_TRACKED:
    Serial.println(F("EV_BEACON_TRACKED"));
    break;
  case EV_JOINING:
    Serial.println(F("EV_JOINING"));
    break;
  case EV_JOINED:
    Serial.println(F("EV_JOINED"));
    LoRaWAN_Tx_Ready =  !(LMIC.opmode & OP_TXDATA); // otherwise joined without TX blocks queue
    break;
    /*
        || This event is defined but not used in the code. No
     || point in wasting codespace on it.
     ||
     || case EV_RFU1:
     ||     Serial.println(F("EV_RFU1"));
     ||     break;
     */
  case EV_JOIN_FAILED:
    Serial.println(F("EV_JOIN_FAILED"));
    break;
  case EV_REJOIN_FAILED:
    Serial.println(F("EV_REJOIN_FAILED"));
    break;
  case EV_TXCOMPLETE:
    Serial.println(F("EV_TXCOMPLETE (includes waiting for RX windows)"));
    if (LMIC.txrxFlags & TXRX_ACK)
      Serial.println(F("Received ack"));
    if (LMIC.dataLen) {
      Serial.println(F("Received "));
      Serial.println(LMIC.dataLen);
      Serial.println(F(" bytes of payload"));
      LoRaWAN_Rx_Payload = 0; // #kgo Payload IoT-Werkstatt
      LoRaWAN_Rx_Port    = LMIC.frame[LMIC.dataBeg-1];              
      for (int i = 0;i<LMIC.dataLen;i++) { 
        Serial.println(LMIC.frame[i+ LMIC.dataBeg],HEX);
        LoRaWAN_Rx_Payload = 256*LoRaWAN_Rx_Payload+LMIC.frame[i+ LMIC.dataBeg];
      }
#ifdef LORA_DOWNLINK_ENABLE 
      LoRaWAN_DownlinkCallback();
#endif 
    }
    LoRaWAN_Tx_Ready =  !(LMIC.opmode & OP_TXDATA);
    // Schedule next transmission
    // os_setTimedCallback(&sendjob, os_getTime()+sec2osticks(TX_INTERVAL), do_send);
    break;
  case EV_LOST_TSYNC:
    Serial.println(F("EV_LOST_TSYNC"));
    break;
  case EV_RESET:
    Serial.println(F("EV_RESET"));
    break;
  case EV_RXCOMPLETE:
    // data received in ping slot
    Serial.println(F("EV_RXCOMPLETE"));
    break;
  case EV_LINK_DEAD:
    Serial.println(F("EV_LINK_DEAD"));
    break;
  case EV_LINK_ALIVE:
    Serial.println(F("EV_LINK_ALIVE"));
    break;
    /*
        || This event is defined but not used in the code. No
     || point in wasting codespace on it.
     ||
     || case EV_SCAN_FOUND:
     ||    Serial.println(F("EV_SCAN_FOUND"));
     ||    break;
     */
  case EV_TXSTART:
    Serial.println(F("EV_TXSTART"));
    break;
  case EV_TXCANCELED:
    Serial.println(F("EV_TXCANCELED"));
    break;
  case EV_RXSTART:
    /* do not print anything -- it wrecks timing */
    break;
  case EV_JOIN_TXCOMPLETE:
    Serial.println(F("EV_JOIN_TXCOMPLETE: no JoinAccept"));
    break;
  default:
    Serial.print(F("Unknown event: "));
    Serial.println((unsigned) ev);
    break;
  }
}
// -- initialize LoraWAN LMIC structure
void LoRaWAN_Start(int fromRTCMem) { // using OTA-Communication 
  os_init();             // LMIC LoraWAN
  LMIC_reset();          // Reset the MAC state 
  LMIC_setClockError(MAX_CLOCK_ERROR * 5 / 100); // timing difference esp clock
  if  (fromRTCMem) { 
#ifdef LORA_DEEPSLEEP 
    LoadLMICFromRTC(); // restart from deepsleep, get LMIC state from RTC 
#endif
  } // continue runing state-maschine
}

extern "C" {  // zur Nutzung der speziellen ESP-Befehle wie Deep Sleep
#include "user_interface.h"
}

void LoRaWAN_DownlinkCallback(){ // ---------- my callbackfunction downlink
  Serial.print("message"+String(String(LoRaWAN_Rx_Payload)));
  Serial.println();
  if (( ( LoRaWAN_Rx_Payload ) == ( 1 ) ))
  {
    if (os_queryTimeCriticalJobs(ms2osticks(1))) { 
      Serial.println("busywaiting for criticalJobs");
      while (os_queryTimeCriticalJobs(ms2osticks(1))) { 
        yield();  
        os_runloop_once();
      }
    }
    SaveLMICToRTC(1/1000); // Save LMIC-State 
    ESP.deepSleep( (long)1*1000UL,WAKE_RF_DEFAULT);//Tiefschlaf, danach Reset und von vorn
  }
}




void setup(){ // Einmalige Initialisierung
  Serial.begin(115200);
  Wire.begin(); // ---- Initialisiere den I2C-Bus 

  if (Wire.status() != I2C_OK) Serial.println("Something wrong with I2C");

  HM330sensor_ready = !HM330sensor.init(); // HM330 Feinstaubsensor 

  WiFi.mode(WIFI_STA); // Pax-counter
  iaqSensor.begin(BME680_I2C_ADDR_PRIMARY, Wire);
  String output = "\nBSEC library version " + String(iaqSensor.version.major) + "." + String(iaqSensor.version.minor) + "." + String(iaqSensor.version.major_bugfix) + "." + String(iaqSensor.version.minor_bugfix);
  Serial.println(output);
  iaqSensor.setConfig(bsec_config_iaq);
  checkIaqSensorStatus();

  bsec_virtual_sensor_t sensorList[10] = {
    BSEC_OUTPUT_RAW_TEMPERATURE,
    BSEC_OUTPUT_RAW_PRESSURE,
    BSEC_OUTPUT_RAW_HUMIDITY,
    BSEC_OUTPUT_RAW_GAS,
    BSEC_OUTPUT_IAQ,
    BSEC_OUTPUT_STATIC_IAQ,
    BSEC_OUTPUT_CO2_EQUIVALENT,
    BSEC_OUTPUT_BREATH_VOC_EQUIVALENT,
    BSEC_OUTPUT_SENSOR_HEAT_COMPENSATED_TEMPERATURE,
    BSEC_OUTPUT_SENSOR_HEAT_COMPENSATED_HUMIDITY,
  };

  iaqSensor.updateSubscription(sensorList, 10, BSEC_SAMPLE_RATE_LP);
  checkIaqSensorStatus();  
  iaqSensor_Housekeeping();
  Bsec_Ticker.attach_ms(3000, iaqSensor_Housekeeping);

  Serial.println();
  LoRaWAN_Start(true); // Prepare LMIC-Engine

}

void loop() { // Kontinuierliche Wiederholung 
  feinstaub = readFeinstaubHM330(2) ;
  uv_strahlung = analogRead(0) ;
  pax = WiFiPaxCounter(-100,60,0,"all mac",false) ;
  temperatur = iaqSensor.temperature ;
  luftfeuchtigkeit = iaqSensor.humidity ;
  Serial.print("Feinstaub:"+String(String(feinstaub)));
  Serial.println();
  Serial.print("UV:"+String(String(uv_strahlung)));
  Serial.println();
  Serial.print("PAX:"+String(String(pax)));
  Serial.println();
  Serial.print("Temperatur:"+String(String(temperatur)));
  Serial.println();
  Serial.print("Luftfeuchtigkeit:"+String(String(luftfeuchtigkeit)));
  Serial.println();

  { //Block------------------------------ send data to network
    int port = 10;
    static uint8_t mydata[15];
    int wert=round(feinstaub*1000);
    mydata[0] = wert >> 16; 
    mydata[1] = wert >> 8; 
    mydata[2] = wert ;
    wert=round(uv_strahlung*1000);
    mydata[3] = wert >> 16; 
    mydata[4] = wert >> 8; 
    mydata[5] = wert ;
    wert=round(pax*1000);
    mydata[6] = wert >> 16; 
    mydata[7] = wert >> 8; 
    mydata[8] = wert ;
    wert=round(temperatur*1000);
    mydata[9] = wert >> 16; 
    mydata[10] = wert >> 8; 
    mydata[11] = wert ;
    wert=round(luftfeuchtigkeit*1000);
    mydata[12] = wert >> 16; 
    mydata[13] = wert >> 8; 
    mydata[14] = wert ;
    int Retry = 3, tout = 5000,  ok=-1;
    while (Retry > 0) {
      // Check if there is not a current TX/RX job running, wait until finished
      if (!((LMIC.opmode & OP_TXRXPEND) || (LMIC.opmode & OP_TXDATA) || (LMIC.opmode & OP_POLL) || (LMIC.opmode & OP_JOINING))) {
        //LoRaWAN_Tx_Ready = 0;
        ok = LMIC_setTxData2(port, mydata, sizeof(mydata), 0);     // Sende  
        if (ok!=0) {
          Serial.println(F("------------------------  setTxData: "));
          Serial.print(ok);
        } 
        else {
          Serial.println(F("Packet queued "));
          while (LMIC.opmode & OP_TXDATA) { //(!LoRaWAN_Tx_Ready) {            
            yield();
            os_runloop_once();
          }      
          Serial.println(F("Packet send "));
          Retry = 0;
        }
      }
      if (ok!=0) {
        Retry=Retry-1;
        Serial.println(F("Retry after timeout"));
        long m = millis();
        while ((millis()-m) < tout) {
          yield();
          os_runloop_once();
        }
      }
    } 

    if ((LMIC.opmode & OP_TXRXPEND) || (LMIC.opmode & OP_TXDATA) || (LMIC.opmode & OP_POLL) || (LMIC.opmode & OP_JOINING)) {
      Serial.print(F("some MAC-TXRX activ, mode = "));
      Serial.println(LMIC.opmode,HEX);
      while ((LMIC.opmode & OP_TXRXPEND) || (LMIC.opmode & OP_TXDATA) || (LMIC.opmode & OP_POLL) || (LMIC.opmode & OP_JOINING)) {
        yield();
        os_runloop_once();
      }
    }
    Serial.println(F("Tx Job finished"));
  } // Block 
  delay( 30000 );
  os_runloop_once(); // LORA LMIC Housekeeping
} //end loop

```

\newpage

# Literaturverzeichnis

<!--
## Referenzen
-->
[^SC-Anwendungsfelder]: [@LPB-BW]
[^UNZiele]: vgl. <https://unric.org/de/17ziele/>
[^SCBuch1]: [@SC-MadeinGermany], S. 659
[^LoRaMember]: [@LoRa-Member]
[^AboutLoRa]: [@LoRa-Übersicht]
[^SCBuch2]: [@SC-MadeinGermany], S. 662
[^SCBuch3]: [@SC-MadeinGermany], S. 665
[^WD-BT]: [@WLAN-DS]
[^UVWerteUmrechnung]: vgl. <https://github.com/ThomasTransboundaryYan/CJMCU-GUVA-S12SD-CJMCU-S12D/commit/ea4eb84a66527b5c7ef9033b4f56638e4620c982>
